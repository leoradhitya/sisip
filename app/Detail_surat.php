<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_surat extends Model
{
    //
    protected $fillable = [
        'surat_id','user_id',
    ];
    public function surat(){
    	return $this->belongsTo('App\Surat');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
