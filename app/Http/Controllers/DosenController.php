<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\User;

class DosenController extends Controller
{
    //
    public function editProfile(){
    	$id = Auth::user()->id;

    	$user = User::find($id);

    	return view('dosen.edit-profil',compact('user'));
    }
}
