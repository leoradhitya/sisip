<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
	
    //
    public function index(){
        if(Auth::check()){
            $user = Auth::user();
            return ($user->role == 'admin') ? redirect('/admin') : redirect('/dosen');
        }else{
            return view('login');
        }
    }
    public function login(Request $req){
        $cred = $req->only('NIDN','password');

        if(Auth::attempt($cred)){
            $user = Auth::user();
            return ($user->role == 'admin') ? redirect('/admin') : redirect('/dosen');
        }else{
            $req->session()->flash('message','Salah NIDN/password');
            return redirect('/');
        }
    }

     public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
