<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Hash;

class InstallController extends Controller
{
    //
    public function install(){
    	//\Artisan::call('migrate:fresh');

    	$adm = new User;

    	$adm->email = 'admin@admin';
    	$adm->nama = 'Administrator';
    	$adm->NIDN = '333';
    	$adm->password = Hash::make('admin');
    	$adm->role = 'admin';

      $adm->save();

    	return 'ok';


    }
}
