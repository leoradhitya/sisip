<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisSurat;
use App\KegiatanSurat;
use App\Surat;
use App\Jenis;
use App\Kegiatan;
use App\User;
use Hash;
use Storage;
use DB;
class SuratController extends Controller
{
    //
    function num2rom($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
    function generateNoSurat($tgl,$jenis,$kegiatan){
        /*$tgl = date("Y-m-d");
        $jenis = '01';
        $kegiatan = "P.E";*/
        $d = date_parse_from_format("Y-m-d", $tgl);
        
        $bulan = $this->num2rom($d['month']);
        $tahun = $d['year'];

        $modelSurat = new Surat();

        $totalJenis = $modelSurat
            ->where(DB::raw('MONTH("tanggal")'),'=',$d['month'])
            ->where('jenis_surat',$jenis)
            ->count();
        $ind = $totalJenis + 1;
        $no = sprintf("%'.02d", $ind).'/'.$jenis.'/LPPM/'.$kegiatan.'/'.$bulan.'/'.$tahun;
        return $no;
    }
    public function all(){
    	$surats= Surat::all();
    	return view('admin.list-surat',compact('surats'));
    }
    public function create(){
    	return view('admin.create-surat');
    }

    function store(Request $r){
        $modelSurat = new Surat();

        $noSurat = $this->generateNoSurat($r->tanggal,$r->jenis,$r->kegiatan);


        
        $modelSurat->no_surat = $noSurat;
        $modelSurat->judul = $r->judul;
        $modelSurat->tanggal = $r->tanggal;
        $modelSurat->jenis_surat = $r->jenis;
        $modelSurat->kegiatan_surat =$r->kegiatan;
        $modelSurat->isi = $r->isi;

        $modelSurat->save();

        return redirect('/admin/surat');

    }

    function jenisAll(){
        $datas = Jenis::all();
        return view('admin.list-jenis', compact('datas'));
    }

    function jenisCreate(){
        return view('admin.create-jenis');
    }

    function jenisStore(Request $r){
        $jenismodel = new Jenis();

        $jenismodel->kode = $r->kode;
        $jenismodel->jenis = $r->jenis;

        $jenismodel->save();

        return redirect('/admin/surat/jenis');
    }

    function kegiatanAll(){
        $datas = Kegiatan::all();
        return view('admin.list-kegiatan', compact('datas'));
    }
    function kegiatanCreate(){
        return view('admin.create-kegiatan');
    }
    function kegiatanStore(Request $r){
        $kgModel = new Kegiatan();

        $kgModel->kode = $r->kode;
        $kgModel->kegiatan = $r->kegiatan;

        $kgModel->save();

        return redirect('/admin/surat/kegiatan');
    }
    public function getDataSurat(){
    	//sementara manual
    	$datas = [];

    	$datas['jenis']=[];
    	$datas['kegiatan']=[];

        $jenismodel = Jenis::all();
        $kgModel = Kegiatan::all();
        $dosen = User::where('role','dosen')->get();

        foreach ($jenismodel as $key => $value) {
            $datas['jenis'][$key]['nama']=$value->jenis;
            $datas['jenis'][$key]['kode']=$value->kode;    
        }

        foreach ($kgModel as $key => $value) {
            $datas['kegiatan'][$key]['nama']=$value->kegiatan;
            $datas['kegiatan'][$key]['kode']=$value->kode;
        }

        foreach ($dosen as $key => $value) {
            $datas['dosen'][$key]['nama']=$value->nama;
            $datas['dosen'][$key]['id']=$value->id;   
        }

    	return json_encode($datas);
    }



    //jenis surat
    public function all_jenis(){
      $jenis= JenisSurat::paginate(100);
      return view('admin.list-jenis',compact('jenis'));
    }
    // public function create_jenis(){
    //   return view('admin.create-jenis');
    // }
    public function create_jenis(Request $req){
    	$model = new JenisSurat;
    	$model->kode = $req->kode_jenis;
    	$model->nama = $req->nama_jenis;


    	$model->save();
    	return redirect('/admin/jenis')->with('success','good!');
    }

    //kegiatan surat
    public function all_kegiatan(){
      $kegiatan= KegiatanSurat::paginate(100);
      return view('admin.list-kegiatan',compact('kegiatan'));
    }

    public function create_kegiatan(Request $req){
    	$model = new KegiatanSurat;
    	$model->kode = $req->kode_kegiatan;
    	$model->nama = $req->nama_kegiatan;

    	$model->save();
    	return redirect('/admin/kegiatan')->with('success','good!');
    }

    public function detailSurat($id){
        $model = Surat::find($id);

        return view('admin.detail-surat',compact('model'));
    }

    public function detailSuratPost(Request $r){
        $modelSurat = Surat::find($r->id);

        if($r->hasFile('surat')){
            if($r->lokasi){
                Storage::delete($r->id);    
            }
            $f = $r->file('surat')->storeAs('public/surat',$r->id);
            $path = Storage::url($f);
            $modelSurat->lokasi = $path;
        }

        $modelSurat->save();

        return redirect('admin/surat');

    }

    function deleteSurat($id){
        $modelSurat = Surat::find($id);
        $modelSurat->delete();
        return redirect('/admin/surat')->with(['success' => 'Data berhasil dihapus']);

    }
}
