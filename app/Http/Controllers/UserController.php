<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

//use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function all(){
        $id = Auth::user()->id;
    	$users = User::where('id','<>',$id)->get();
    	return view('admin.list-user', compact('users'));
    }

    public function findUserJSON($id){
       $user = User::find($id);

       return $user->toJson();
    }

    public function create(Request $req){
    	$model = new User;
    	$model->nama = $req->nama;
    	$model->NIDN = $req->NIDN;
    	$model->email = $req->email;
    	$model->role = $req->role;
    	$model->password = Hash::make($req->NIDN);

    	$model->save();
    	return redirect('/admin/user')->with('success','good!');
    }

    public function editForm($id){
        $user = User::find($id);

        return view('admin.edit-user',compact('user'));
    }

    public function editProses(Request $req){
        $nidn = $req->NIDN;

        $model = User::where('NIDN',$nidn)->first();
        
        $model->nama = $req->nama;
        $model->NIDN = $req->NIDN;
        $model->email = $req->email;
        $model->save();

        return redirect('/admin/user');

    }
    public function resetPassword($id){
        $user = User::find($id);
        $user->password = Hash::make($req->NIDN);
        return redirect('/admin/user');
    }
    public function deleteUser($id){
        $user = User::find($id);
        $user->delete();

        return redirect('/admin/user')->with('success','Data telah dihapus.');
    }

    // public function import()
    // {
    //     Excel::import(new UsersImport, 'users.xlsx');
    //
    //     return redirect('/')->with('success', 'All good!');
    // }
    public function import(Request $req)
    {
         Excel::import(new UsersImport, $req->fileimport);

        return redirect('/admin/user')->with('success', 'All good!');
    }
}
