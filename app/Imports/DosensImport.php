<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;

class UsersImport implements ToModel
{
    
    public function model(array $row)
    {
        return new User([
         'NIDN'     => $row[0],
         'nama'    => $row[1],
         'email'    => $row[2],
         'password' => Hash::make($row[0]),
         'role' => 'dosen'
        ]);
    }

    
}
