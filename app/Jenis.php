<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    //
    protected $fillable = [
        'kode','jenis',
    ];

    protected $table = "jenis";
}
