<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KegiatanSurat extends Model
{
    //
    protected $fillable = ['nama','kode'];
}
