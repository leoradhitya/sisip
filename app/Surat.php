<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    //
    protected $fillable = [
        'no_surat','judul','tanggal','jenis_surat','kegiatan_surat','lokasi','isi'
    ];
    
    public function detailSurat(){
    	return $this->hasMany('App\Detail_surat');
    }
}
