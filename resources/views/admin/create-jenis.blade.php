@extends('adminlte::page')

@section('title', 'Jenis Surat Baru')

@section('content_header')
    <h1>Jenis Surat Baru</h1>
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
		<form action="" method="POST" class="">
			{{ csrf_field() }}
			
			<div class="form-group">
				<label>Kode Surat: </label>
				<input class="form-control" type="text" name="kode" placeholder="Kode Surat">
			</div>
			<div class="form-group">
				<label>Jenis Surat: </label>
				<input class="form-control" type="text" name="jenis" placeholder="Jenis Surat"> 
			</div>
			<input type="submit" name="save" value="Save" class="btn btn-primary">
			
		</form>	
	</div>
</div>
@stop