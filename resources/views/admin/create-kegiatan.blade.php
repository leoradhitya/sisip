@extends('adminlte::page')

@section('title', 'Jenis Kegiatan Baru')

@section('content_header')
    <h1>Jenis Kegiatan Baru</h1>
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
		<form action="" method="POST" class="">
			{{ csrf_field() }}
			
			<div class="form-group">
				<input class="form-control" type="text" name="kode" placeholder="Kode Kegiatan">
			</div>
			<div class="form-group">
				<input class="form-control" type="text" name="kegiatan" placeholder="Jenis Kegiatan"> 
			</div>
			<input type="submit" name="save" value="Save" class="btn btn-primary">
			
		</form>	
	</div>
</div>
@stop