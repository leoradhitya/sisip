@extends('adminlte::page')

@section('title', 'Surat Baru')

@section('content_header')
    <h1>Detail Surat</h1>
@stop

@section('content')

<form action="{{ url('/admin/surat/edit') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
	<h4>No Surat: {{ $model->no_surat }}</h4>
	<div class="row">
		<fieldset class="col-md-6">
			<input type="hidden" name="id" value="{{ $model->id }}">
			
			<div class="form-group">
				<label>Judul:</label>
				<input class="form-control" type="text" name="judul" placeholder="Judul" value="{{ $model->judul }}"> 
			</div>
			<div class="form-group">
				<label>Tanggal:</label>
				<input type='text' name="tanggal" class="form-control" id='datepicker' placeholder="Tanggal" value="{{ $model->tanggal }}" />
			</div>
		</fieldset>
	
	
		<fieldset class="col-md-6">
			<div class="form-group">
				<label>Jenis Surat:</label>
				<select id="jenis-select" class="form-control" name="jenis">
					
				</select>
			</div>
			<div class="form-group">
				<label>Jenis Kegiatan:</label>
				<select id="kegiatan-select" class="form-control" name="kegiatan">
					
				</select>
			</div>
		</fieldset>
	</div>
	<!-- <div class="row">
		<fieldset class="col-md-12">
			<div class="form-group">
				<label>Isi:</label>
				<textarea name="isi" id="editor" class="form-control" rows="15
				"></textarea>
			</div>
		</fieldset>
	</div> -->
	<div class="row">
		<fieldset class="col-md-12">
			<div class="form-group">
				<label>Unggah Dokumen:</label>
		  		<input type="file" class="form-control" id="upload" name="surat">
			</div>
		</fieldset>
		
	</div>
	
	<a href="{{ url('/admin/surat/cetak/{id}') }}" class="btn btn-success">Cetak</a>
	<input type="submit" name="save" value="Save" class="btn btn-primary">
	
</form>	
@stop
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" />
@stop
@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script>
<script type="text/javascript">

	$(function(){
		$('#datepicker').datepicker({
			dateFormat: 'yy-mm-dd'
		});

		$.get("{{ url('/admin/surat/get-data') }}", function(d){
			datas = JSON.parse(d);
			
			for (var i = 0; i < datas.jenis.length; i++) {
				var nama=datas.jenis[i].nama;
				var kode=datas.jenis[i].kode;
				$('#jenis-select').append('<option value="'+kode+'">'+nama+'</option>');
			}

			$('#jenis-select').val('{{ $model->jenis_surat }}');
			for (var i = 0; i < datas.kegiatan.length; i++) {
				var nama=datas.kegiatan[i].nama;
				var kode=datas.kegiatan[i].kode;
				$('#kegiatan-select').append('<option value="'+kode+'">'+nama+'</option>');
			}
			$('#kegiatan-select').val('{{ $model->kegiatan_surat }}');

			// for (var i = 0; i < datas.dosen.length; i++) {
			// 	var nama=datas.dosen[i].nama;
			// 	var kode=datas.dosen[i].id;
			// 	$('#dosen-select').append('<option value="'+kode+'">'+nama+'</option>');
			// }

			$('#jenis-select').select2();
			$('#kegiatan-select').select2();
			$('#dosen-select').select2();
			ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
			

		});
	});
</script>
@stop