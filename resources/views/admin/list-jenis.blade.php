@extends('adminlte::page')

@section('title', 'List Jenis Surat')

@section('content_header')
    <h1>Manage Jenis Surat</h1>
@stop
@section('content')
@include('layout.flash')
<div class="panel panel-default">
	<div class="panel-body">
		<div class="col-sm-6" align="left">
			<a href="{{ url('admin/surat/jenis/baru') }}" class="btn btn-success"><i class="fas fa-plus" ></i> JENIS SURAT</a>
		</div>
		
	</div>
<div class="table-condensed panel-body">
<table class="table" id="newTable">
	<thead>
		<tr>
		<th>Kode</th>
		<th>Jenis Surat</th>
		<th>Aksi</th>
		</tr>
	</thead>
	<tbody id="myTable">

	@forelse($datas as $data)
		<tr>
			<td>
				{{ $data->kode }}
			</td>
			<td>
				{{ $data->jenis }}
			</td>
			<td>
				<a href="{{ url('admin/surat/jenis/detail/').$data->id }}" class="btn btn-primary" >Detail</a>
			
			</td>
		</tr>
	@empty
	<tr>
		
		<td></td>
		<td>No Data</td>
		<td></td>
		
	</tr>
	@endforelse
		
	</tbody>
</table>
</div>
</div>
@stop
@section('js')
<script type="text/javascript">
	$(function(){
		$('#newTable').DataTable();

	});
</script>
@stop

