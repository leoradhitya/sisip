@extends('adminlte::page')

@section('title', 'List Surat')

@section('content_header')
    <h1>Manage Surat</h1>
@stop
@section('content')
@include('layout.flash')
<div class="panel panel-default">
	<div class="panel-body">
		<div class="col-sm-6" align="left">
			<a href="{{ url('admin/surat/baru') }}" class="btn btn-success"><i class="fas fa-plus" ></i> SURAT</a>
		</div>
		
	</div>
<div class="table-condensed panel-body">
<table class="table" id="newTable">
	<thead>
		<tr>
		<th>No. Surat</th>
		<th>Judul</th>
		<th>Tanggal</th>
		
		<th>Jenis</th>
		<th>Kegiatan</th>
		<th>Lokasi</th>
		<th>Action</th>
		</tr>
	</thead>
	<tbody id="myTable">

	@forelse($surats as $surat)
		<tr>
			<td>
				{{ $surat->no_surat }}
			</td>
			<td>
				{{ $surat->judul }}
			</td>
			<td>
				{{ $surat->tanggal }}
			</td>
			
			<td>
				{{ $surat->jenis_surat }}
			</td>
			<td>
				{{ $surat->kegiatan_surat }}
			</td>
			<td>
				@if($surat->lokasi)
				<a href="{{ url($surat->lokasi) }}">file</a>
				@else
				no file
				@endif
			</td>
			<td>
				<a href="{{ url('admin/surat/detail').'/'.$surat->id }}" class="btn btn-primary" >Detail</a>
				<a href="{{ url('admin/surat/delete').'/'.$surat->id }}" class="btn btn-danger" onclick="return confirm('Hapus surat?');">Hapus</a>
		
			</td>
		</tr>
	@empty
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td>No Data</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	@endforelse
		
	</tbody>
</table>
</div>
</div>
@stop
@section('js')
<script type="text/javascript">
	$(function(){
		$('#newTable').DataTable();

	});
</script>
@stop

