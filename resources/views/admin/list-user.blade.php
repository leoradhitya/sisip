@extends('adminlte::page')

@section('title', 'List User')

@section('content_header')
    <h1>Manage User</h1>
@stop
@section('content')
@include('layout.flash')
<div class="panel panel-default">
	<div class="panel-body">

		<div class="col-sm-6" align="left">
			<button class="btn btn-primary" data-toggle="modal" data-target="#form-tambah"><i class="fas fa-plus" ></i> USER</button>
			<button class="btn btn-success" data-toggle="modal" data-target="#form-import"><i class="fas fa-file-import"></i> IMPORT FILE</button>
		</div>
	</div>
<div class="table-condensed panel-body">
<table class="table" id="newTable">
	<thead>
		<tr>
		<th>No.</th>
		<th>NIDN</th>
		<th>Nama</th>
		<th>Email</th>
		<th>Action</th>
		</tr>
	</thead>
	<tbody>

	@forelse($users as $user)
		<tr>
			<td>
				{{ $loop->index + 1 }}
			</td>
			<td>
				{{ $user->NIDN }}
			</td>
			<td>
				{{ $user->nama }}
			</td>
			<td>
				{{ $user->email }}
			</td>
			<td>
				<button class="btn btn-primary" data-toggle="modal" data-target="#form-detail-user" data-id="{{ $user->id }}">Detail</button>
				<!-- <a href="/admin/user/{{ $user->id }}" class="btn btn-success">Detail</a> -->
			</td>
		</tr>
	@empty
	<tr>
		<td colspan="4">No Data</td>
	</tr>
	@endforelse
		
	</tbody>
</table>
</div>
<!-- modal tambah -->
<div id="form-tambah" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah <strong>USER</strong></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>

			</div>
			<div class="modal-body">
				<form action="/admin/user" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<input class="form-control" type="text" name="NIDN" placeholder="NIDN">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="nama" placeholder="Nama">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="email" placeholder="Email">
					</div>
					<div class="form-group" align="right">
						<label class="radio-inline">
					      	<input type="radio" name="role" value="dosen" checked>Dosen
					    </label>
					    <label class="radio-inline">
					      	<input type="radio" name="role" value="admin">Admin
					    </label>
					</div>
			</div>
			<div class="modal-footer">
				<input type="submit" name="save" value="Save" class="btn btn-primary">
				</form>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- start modal import -->
<div id="form-import" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Import <strong>USER</strong></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>

			</div>
			<div class="modal-body">
				<form action="/admin/userimport" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<input class="form-control" type="file" name="fileimport"> File type *.xls
					</div>
			</div>
			<div class="modal-footer">
				<input type="submit" name="save" value="Save" class="btn btn-primary">
				</form>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Detail -->

<div id="form-detail-user" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Detail <strong>USER</strong></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				
				<table class="table table-borderless">
					<tbody>
						<tr>
							<th>NIDN:</th>
							<td id="user-nidn"></td>
						</tr>
						<tr>
							<th>Nama:</th>
							<td id="user-name"></td>
						</tr>
						<tr>
							<th>Email:</th>
							<td id="user-email"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-danger" id="user-hapus">Hapus</a>
				<a href="#" class="btn btn-success" id="user-edit">Edit</a>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
    

@stop

@section('js')

<script>
function confirmDialog(message, handler){
	$('<div class="modal fade" id="promptModal" role="dialog"><div class="modal-dialog"> <div class="modal-content"> <div class="modal-body" style="padding:10px;"> <h4 class="text-center">'+message+'</h4> <div class="text-center"> <a class="btn btn-danger btn-yes">yes</a> <a class="btn btn-default btn-no">no</a> </div> </div> </div> </div>  </div>').appendTo('body');
	
	$('#promptModal').modal({
		backdrop: 'static',
		keyboard: false
	});

	//pass value
	$(".btn-yes").click(function () {
	   handler(true);
	   $("#promptModal").modal("hide");
	});

	$(".btn-no").click(function () {
	   handler(false);
	   $("#promptModal").modal("hide");
	});

	$("#promptModal").on('hidden.bs.modal', function () {
      $("#promptModal").remove();
   });

}
function promptHapus(id){
	confirmDialog("Hapus?",(x)=>{
		if(x){
			window.location.replace('{{ url("admin/user/hapus") }}/'+ id)
			//console.log(id + x);
		}	
	});
}

$(document).ready(function(){
	$("#form-detail-user").on('show.bs.modal', function(e){
		var btn = $(e.relatedTarget);
		var id = btn.data('id');
		var modal = $(this);
		$.get("{{ url('/admin/user') }}/"+id, function(data){
			user = JSON.parse(data);
			$("#user-name").text(user.nama);
			$("#user-nidn").html(user.NIDN);
			$("#user-email").html(user.email);
			modal.find("#user-hapus").attr("href",'javascript:promptHapus("'+id+'")');
			modal.find("#user-edit").attr("href",'{{ url("admin/user/edit") }}/'+ id);
			//console.log(JSON.parse(data));
		});
	});
	$('#newTable').DataTable();
});
</script>
@stop