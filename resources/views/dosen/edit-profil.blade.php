@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit User</h1>
@stop

@section('content')

<div class="row mt-2">
	<div class="col-md-6">
		<form action="{{ url('/admin/user/edit') }}" method="POST" class="mt-2">
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $user->id }}">
			<div class="form-group">
				<label>NIDN: </label>
				<input class="form-control" type="text" name="NIDN" placeholder="NIDN" value="{{ $user->NIDN }}">
			</div>
			<div class="form-group">
				<label>Nama: </label>
				<input class="form-control" type="text" name="nama" placeholder="Nama" value="{{ $user->nama }}"> 
			</div>
			<div class="form-group">
				<label>Email: </label>
				<input class="form-control" type="text" name="email" placeholder="Email" value="{{ $user->email }}">
			</div>
			<input type="submit" name="save" value="Save" class="btn btn-primary">
			
				<a href="{{ url('/admin/user/reset/').'/'.$user->id }}" class="btn btn-success">Reset password</a>
			
		</form>	
	</div>
</div>
				
@stop