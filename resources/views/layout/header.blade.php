<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="/">SISTEM <strong>ARSIP</strong></a>
	@if(Auth::check())
	<a href="/logout" class="btn btn-dark ml-auto bg-dark">Logout</a>
	@endif
</nav>