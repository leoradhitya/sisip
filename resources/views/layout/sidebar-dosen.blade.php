    
    <!-- Sidebar -->
    <nav id="sidebar" class="sidebar-wrapper navbar-dark bg-dark">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#">SISTEM ARSIP</a>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
            alt="User picture">
        </div>

        <div class="user-info">
          <span class="user-name">
            <strong>{{ Auth::user()->nama }}</strong>
          </span>
          <span class="user-role">{{ Auth::user()->role }}</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>Manage</span>
          </li>
          <li>
            <a href="/dosen/profil">
              <i class="fas fa-users"></i>
              <span>Manage profil</span>
            </a>
          </li>
          <li>
            <a href="/dosen/surat">
              <i class="fas fa-envelope-open-text"></i>
              <span>Request surat</span>
            </a>
          </li>
        </ul>
      </div>
      <!-- sidebar-menu  -->
    
    
    
  </nav>
