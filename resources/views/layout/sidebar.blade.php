
    <!-- Sidebar -->
    <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#">SISTEM ARSIP</a>
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
            alt="User picture">
        </div>

        <div class="user-info">
          <span class="user-name">
            <strong>{{ Auth::user()->nama }}</strong>
          </span>
          <span class="user-role">{{ Auth::user()->role }}</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>

      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>Manage</span>
          </li>
          <li>
            <a href="/admin/user">
              <i class="fas fa-users"></i>
              <span>Manage User</span>
            </a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fas fa-envelope-open-text"></i>
              <span>Surat</span>
              <span class="sr-only">(current)</span><span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li style="background-color:#343A40; color: #818896;">
                <a href="/admin/surat">
                  <i class="fas fa-envelope-open-text"></i>
                  <span>Manage Surat</span>
                </a>
              </li>
              <li style="background-color:#343A40; color: #818896;">
                <a href="/admin/kegiatan">
                  <i class="fas fa-envelope-open-text"></i>
                  <span>Manage Kegiatan</span>
                </a>
              </li>
              <li style="background-color:#343A40; color: #818896;">
                <a href="/admin/jenis">
                  <i class="fas fa-envelope-open-text"></i>
                  <span>Manage Jenis Surat</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->

  </nav>
@include('js')
