<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
	<link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
@if(Auth::check())

@endif

<div class="wrapper">
	<div id="header">
	@include('layout.header')
	</div>
@if(Auth::check())
    @if(Auth::user()->role == 'admin')
    	@include('layout.sidebar')
    @else
    	@include('layout.sidebar-dosen')
    @endif
@endif
<div id="content">
	@if(Auth::check())
	
	@endif
	<div class="container-fluid">
		@include('js')
	@yield('content')
	</div>
</div>
</div>
@if(Auth::check())


@endif
</body>
</html>