@extends('master')
@section('content')

  <div class="card mx-auto border-0">
    <div class="card-header border-bottom-0 bg-transparent text-center">
     <h2><strong>SISTEM</strong> ARSIP</h2>
    </div>

    <div class="card-body pb-4">
      <div class="tab-content" id="pills-tabContent">
      	@if(Session::has('message'))
      	<div class="alert alert-danger">
      		<strong>Gagal!</strong> {{ Session::get('message') }}
      	</div>
      	@endif
        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="pills-login-tab">
          <form action="{{ url('/login') }}" method="POST">
          	{{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="NIDN" class="form-control" id="NIDN" placeholder="NIDN" required autofocus>
            </div>

            <div class="form-group">
              <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
            </div>

            <div class="text-center pt-4">
              <button type="submit" class="btn btn-primary">Login</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection