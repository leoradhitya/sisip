<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
	<link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet">

</head>
<body>
<div class="page-wrapper chiller-theme">
	
@if(Auth::check())
@include('layout.header')
<a id="show-sidebar" class="burger p-t-30 btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    @include('layout.sidebar')
@endif
<div class="container">
	
	@yield('content')
</div>
</div>

</body>
</html>