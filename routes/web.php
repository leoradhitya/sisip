<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/d','SuratController@generateNoSurat');

Route::get('/install','InstallController@install');

Route::get('/','HomeController@index');

Route::get('/login', function(){
	return redirect('/');
});
Route::get('/logout','HomeController@logout');
Route::post('/logout','HomeController@logout');
Route::get('/register', function(){
	return redirect('/');
});


Route::post('/login','HomeController@login');


//admin
Route::group(['prefix' => 'admin',  'middleware' => 'checkadmin'], function(){
	// semua url /admin/.. digroup sini

	Route::get('/',function(){
		return view('admin.index');
	});
	///user
	Route::get('/user','UserController@all');
	Route::post('/user','UserController@create');
	Route::get('/user/{id}','UserController@findUserJSON');
	Route::get('/user/hapus/{id}','UserController@deleteUser');
	Route::get('/user/edit/{id}','UserController@editForm');
	Route::post('/user/edit','UserController@editProses');
	Route::get('/userimport','UserController@import');
	Route::post('/userimport','UserController@import');
	Route::get('/user/reset/{id}','UserController@resetPassword');

	///surat
	Route::get('/surat','SuratController@all');
	Route::get('/surat/baru','SuratController@create');
	Route::post('/surat/baru','SuratController@store');
	Route::get('/surat/get-data','SuratController@getDataSurat');
	Route::get('/surat/detail/{id}','SuratController@detailSurat');
	Route::post('/surat/edit','SuratController@detailSuratPost');
	Route::get('/surat/delete/{id}','SuratController@deleteSurat');

	//jenis surat
	Route::get('/surat/jenis','SuratController@jenisAll');
	Route::get('/surat/jenis/baru','SuratController@jenisCreate');
	Route::post('/surat/jenis/baru','SuratController@jenisStore');


	//jenis Kegiatan
	Route::get('surat/kegiatan','SuratController@kegiatanAll');
	Route::get('surat/kegiatan/baru','SuratController@kegiatanCreate');
	Route::post('surat/kegiatan/baru','SuratController@kegiatanStore');
});

//dosen
Route::group(['prefix' => 'dosen',  'middleware' => 'checkdosen'], function(){
	// semua url /admin/.. digroup sini

	Route::get('/',function(){
		return view('dosen.index');
	});
	Route::get('/profil','DosenController@editProfile');
});
